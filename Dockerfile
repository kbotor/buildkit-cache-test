# syntax=docker/dockerfile:1.5
FROM alpine:latest

RUN <<EOF
  set -e
  apk add --no-cache sudo git bash
EOF