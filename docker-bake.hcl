variable "CI_REGISTRY_IMAGE" {}
variable "CI_COMMIT_SHA" {}

variable "tag" {
  default = "latest"
}

group "default" {
  targets = ["build"]
}

target "build" {
  dockerfile = "Dockerfile"
  tags = [
    "${CI_REGISTRY_IMAGE}:${tag}",
    "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}"
  ]
  cache-from = [ "type=registry,ref=${CI_REGISTRY_IMAGE}/cache:${tag}" ]
  cache-to = [ "type=registry,mode=max,ref=${CI_REGISTRY_IMAGE}/cache:${tag}" ]
}